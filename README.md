# Smoothing IOU

[![license](https://img.shields.io/github/license/mashape/apistatus.svg)](LICENSE)

This repository contains a reference implementation of 'Smoothing IoU function described in paper ntersection over Union with smoothing
for bounding box regression'. Pull request are welcomed.

## Implementation
See [reference_implementation_tf](https://gitlab.com/irafm-ai/smoothing-iou/-/blob/main/reference_implementation_tf.ipynb).


## Abstract

> We are focusing on the construction of a loss function for the bounding box regression. The Intersection over Union (IoU) metric is improved to converge faster, to make the surface of the loss function smooth and continuous over the whole searched space, and to reach a more precise approximation of the labels. The main principle is adding a smoothing part to the original IoU, where the smoothing part is given by a linear space with values that increases from the ground truth bounding box to the border of the input image, and thus covers the whole spatial search space. We show the motivation and formalism behind this loss function and experimentally prove that it outperforms IoU, DIoU, CIoU, and SIoU by a large margin. Moreover, we experimentally show that the proposed loss function is robust with respect to the noise in the dimension of ground truth bounding box where by adding such noise, the test accuracy decreases only negligible.

## Requirements
Tensorflow 2.x
